#!/bin/bash

print_usage() {
	echo "Usage: patch-conflict [options] [patch-file]"
	echo "Diffs the patch's \"old\" content with the destination file content,"\
	     "useful to find the reason why the patch doesn't apply."
	echo "Options:"
	echo "  -h | --help        Print this help text"
	echo "  -F | --file <N>    Choose the patch's file number to be compared (by default: 1)"
	echo "                     A substring of the file path can also be passed, instead of a number"
	echo "  -# | --hunk <N>    Choose the patch's hunk number to be compared (by default: 1)"
	echo "  -l | --line <N>    Line number of the destination file where the text to be compared starts."
	echo "                     If omitted, we'll try to guess, but the guessing won't be really reliable"
	echo "  -p | --strip <N>   Trailing path elements to be stripped from patch's filenames (by default: 1)"
	echo "       --diff  <cmd> Diff command to use (by default: 'wdiff -n' if installed, 'diff -su' if not')"
	echo "       --[no-]color  Output with/without colors (requires colordiff installed, by default: auto)"
	echo "  [patch-file]       Path to the patch. If missing, the output of \`quilt next\` will be used."
	echo "The path to the destination file will be extracted from the patch."
	echo "The line at the destination file will be guessed trying to apply the patch (dry-run) with increasing"\
	     "fuzz level (from 0 to 2), and if it fails, just getting the line number from the patch."\
	     "Since this is very prone to make a wrong guess, --line argument will be frequently needed."
	echo "Required packages: patchutils. Recommended packages: wdiff, colordiff"
}

exit_error() {
	echo "Error: $1" >&2
	exit 1
}

# parse args
if [[ -x "$(command -v getopt)" ]]; then
	SHORT_OPTS="h#:F:p:l:"
	LONG_OPTS="help,hunk:,file:,strip:,line:,diff:,color,no-color"
	if ! parsed_args="$(getopt --name "$0" -o "$SHORT_OPTS" -l "$LONG_OPTS" -- "$@")"; then
		print_usage
		exit 1
	fi
	eval set -- "$parsed_args"
fi

while (( $# > 0 )); do
	case "$1" in
		-h|--help)
			print_usage
			exit;;
		-#|--hunk)
			HUNK_NUM="$2"
			shift;;
		-F|--file)
			FILE="$2"
			shift;;
		-p|--strip)
			STRIP="$2"
			shift;;
		-l|--line)
			DST_LINE="$2"
			shift;;
		--diff)
			DIFF_CMD="$2"
			shift;;
		--color)
			COLOR=true
			;;
		--no-color)
			COLOR=false
			;;
		--)
			;;
		*)
			[[ -n $patch_path ]] && exit_error "Please provide only one patch"
			patch_path="$1"
			;;
	esac
	shift
done

# dependencies
[[ -x "$(command -v filterdiff)" ]] || patchutils_installed=true || patchutils_installed=false
[[ -x "$(command -v wdiff)" ]] && wdiff_installed=true || wdiff_installed=false
[[ -x "$(command -v colordiff)" ]] && colordiff_installed=true || colordiff_installed=false
[[ -x "$(command -v quilt)" ]] && quilt_installed=true || quilt_installed=false

# sanity checks
[[ $patchutils_installed == false ]] && exit_error "patchutils is not installed"
[[ -z $patch_path && $quilt_installed == false ]] && exit_error "Please pass the patch file's path"

# default args
HUNK_NUM=${HUNK_NUM:-1}
FILE=${FILE:-1}
STRIP=${STRIP:-1}
COLOR=${COLOR:-auto}
if [[ -z $DIFF_CMD ]]; then
	[[ $wdiff_installed == true ]] && DIFF_CMD="wdiff -n" || DIFF_CMD="diff -su"
fi
if [[ -z $patch_path ]]; then
	if ! patch_path="$(quilt next 2>/dev/null)"; then
		exit_error "Please pass the patch file's path"
	fi
	echo "Argument patch_path not provided, using '$patch_path'"
fi

# helper functions
try_patch() { # $1 = fuzz level
	dst_line=$(patch -p1 -f --dry-run --fuzz="$1" <<< "$hunk_content" | awk '/succeeded/ {print $5}')
	if [[ -n $dst_line ]]; then
		echo "Patch applies at line $dst_line with fuzz $1"
		return 0
	fi
	return 1
}

# get file number
if [[ -z ${FILE//[0-9]} ]]; then  # FILE is number
	file_num="$FILE"
else
	files_count="$(lsdiff -N "$patch_path" | grep -c "$FILE")"
	(( files_count == 0 )) && exit_error "No files match the --file argument"
	(( files_count > 1 )) && exit_error "More than one file match the --file argument"
	file_num="$(lsdiff -N "$patch_path" | grep "$FILE" | awk '{print substr($2, 2)}')"
fi

# get patch hunk content
hunk_content=$(filterdiff -# "$HUNK_NUM" -F "$file_num" "$patch_path")
hunk_old_content=$(tail -n +6 <<< "$hunk_content" | awk '/^[ -]/ {print}' | cut -c 2-)
hunk_old_content_LINES=$(wc -l <<< "$hunk_old_content")

# if --line wasn't provided, try to guess simulating to pach with increasing fuzz level
dst_line=$DST_LINE
if [[ -z $dst_line ]]; then
	echo "Argument --line not provided, trying to guess"
	try_patch 0 || try_patch 1 || try_patch 2

	# last resort, get line from hunk header
	if [[ -z $dst_line ]]; then
		dst_line=$(sed -n '5s/@@ -\([0-9]*\).*/\1/p' <<< "$hunk_content")
		echo "Cannot guess, using line $dst_line extracted from hunk header"
	fi
fi

# get path and content of the patched file
dst_path=$(lsdiff --strip="$STRIP" -# "$HUNK_NUM" -F "$file_num" "$patch_path")
dst_content=$(tail -n +"$dst_line" "$dst_path" | head -n "$hunk_old_content_LINES")

# output format
[[ $COLOR == true && $colordiff_installed == false ]] && echo "colordiff not installed, --color ignored" >&2
[[ $COLOR == auto && -t 1 ]] && COLOR=true || COLOR=false
[[ $COLOR == true && $colordiff_installed == true ]] && out_cmd="colordiff" || out_cmd="cat"

# compare
echo "Comparing hunk $HUNK_NUM with file '$dst_path' line $dst_line"
echo "--- (patch) $patch_path"
echo "+++ (file)  $dst_path"
$DIFF_CMD <(cat <<< "$hunk_old_content") <(cat <<< "$dst_content") | $out_cmd
