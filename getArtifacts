#!/usr/bin/bash
# SPDX-License-Identifier: GPL-2.0

# The pipeline bridge names can be found by going to the MR, clicking the little
# progress boxes for CI, and then clicking the pipeline ID under "Upstream".  DO
# NOT USE THE LINK AT THE TOP OF THE PAGE.

# This only works with separate git trees atm.  TODO: add --release [rhelX|centos|ark] ?

# From there, the left hand column will contain the bridge names.  These are used in the
# RHEL8 and RHEL9 --bridge options below.

usage() {
	echo "getArtifacts will either download the artifact.zip containing the kernel build results, or a URL for downloading results."
	echo "usage: getArtifacts [options] <mrID>"
	echo "       -a: download a specific architecture (default: x86_64)"
	echo "       -k: output the kernel version"
	echo " "
}

[[ $(type -P lab) ]] ||  { echo "lab must be installed and in \$PATH" 1>&2; exit 1; }

dir=$(pwd)
# TODO, add RHEL7 and RHEL6?
[[ "$dir" == *"rhel-8"* ]] && bridgeID="rhel8_merge_request"
[[ "$dir" == *"rhel-9"* ]] && bridgeID="rhel9_merge_request"
[[ "$dir" == *"centos-stream-9"* ]] && bridgeID="c9s_merge_request"
[[ "$dir" == *"kernel-ark"* ]] && bridgeID="ark_merge_request"

# set default arch to x86_64
_arch=x86_64
# do not output nvr by default
output_nvr=0

# optargs is nice but it only allows character use and not strings ...
while [[ $# -gt 0 ]]
do
	key="$1"
	shift
	case $key in
		-a|--arch)
			_arch=$1
			shift;;
		-k|--kernel-version)
			output_nvr=1
			;;
		-h|--help)
			usage
			exit 1
			;;
		*)
			[ ! -z "$mrID" ] && echo "Error: Unknown Merge Request ID ($key)" && usage && exit 1
			mrID=$key
			;;
	esac
done

# some simple error checking
[ -z $mrID ] && echo "Error: Provide a MR ID." && usage && exit 1
re='^[0-9]+$'
if ! [[ $mrID =~ $re ]] ; then
   echo "Error: MR ID must be a number"; exit 1
fi

# works for both rhel-8 and cs9 on Mar 25 2022
if ! lab ci artifacts --merge-request --follow --bridge "$bridgeID" ${mrID}:"build ${_arch}" >& /dev/null; then
	echo "Error: lab ci artifacts failed ... are there artifacts for MR ${mrID}?"
	exit 1
fi

# If the rpms are in the archive, then quit.
if zip -sf artifacts.zip | grep kernel; then
	echo "kernel artifacts downloaded in artifacts.zip"
	exit 1
fi

if [ $output_nvr -eq 1 ]; then
	unzip -p artifacts.zip rc| grep kernel_version | sed "s/=/:/"
fi
# Output the S3 link in artifacts-meta.json
echo -n "S3URL : "
urlpath=$(unzip -p artifacts.zip artifacts-meta.json | grep s3_browse_url | cut -d" " -f4 | tr -d "\",")
echo "${urlpath}/artifacts"
rm -rf artifacts.zip
